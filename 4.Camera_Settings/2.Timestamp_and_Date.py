import cv2
import datetime as dt

# Creating a object Cap of Class Video capture to read video from WebCam
cap = cv2.VideoCapture(0)                

# If the video has opened succesfully then execute this
while(cap.isOpened()):                       

    # ret recieves if the image is recieved or not (True / False)
    # frame captures image frame recieved from video
    ret, frame = cap.read()

    # If the frame is recieved
    if(ret == True):

        # Setting the font to be used for the printting the date and time
        font = cv2.FONT_HERSHEY_SIMPLEX

        # Str object containig the date and time of today
        dnt = str(dt.datetime.now())

        # PAsting the text on the image 
        cv2.putText(frame, dnt, (400, 470), font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

        # Show the video frame
        cv2.imshow('Video Feed', frame)    

        # If the S button is pressed exit the video
        if((cv2.waitKey(1) & 0xFF) == ord('s')):        
            break;

    # Else break and exit the while loop
    else:
        break

# Disable the camera and free up all the occupies resources
cap.release()        

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()   