import cv2

# Creating a object Cap of Class Video capture to read video from WebCam
cap = cv2.VideoCapture(0)                

'''
    Name of the Parameter       Number Associated
    cv2.CAP_PROP_FRAME_WIDTH            3
    cv2.CAP_PROP_FRAME_HEIGHT           4
'''

# Setting the Frame Width of the video to 1920 pixels
cap.set(3, 400)

# Setting the Frame Height of the video to 1080 pixels
cap.set(4, 400)

# Displaying the set parameters
print(cap.get(3), cap.get(4))

# If the video has opened succesfully then execute this
while(cap.isOpened()):                       

    # ret recieves if the image is recieved or not (True / False)
    # frame captures image frame recieved from video
    ret, frame = cap.read()

    # If the frame is recieved
    if(ret == True):

        # Taking Gray Scale image
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Show the video frame
        cv2.imshow('Video Feed', gray)    

        # If the S button is pressed exit the video
        if((cv2.waitKey(1) & 0xFF) == ord('s')):        
            break;

    # Else break and exit the while loop
    else:
        break

# Disable the camera and free up all the occupies resources
cap.release()        

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()   