import cv2

# Creating a object Cap of Class Video capture to read video from WebCam
cap = cv2.VideoCapture(0)                

# Setting the correct video for the Webcam video
#fourcc = cv2.VideoWriter_fourcc(*'XVID')

# Creating an Video Writer Object Output for aving video
#Output = cv2.VideoWriter('2.Reading,Showing,Writing_Videos/Recorded_Video.mp4', fourcc, 20.0, (640, 480))

# If the video has opened succesfully then execute this
while(cap.isOpened()):                       

    # ret recieves if the image is recieved or not (True / False)
    # frame captures image frame recieved from video
    ret, frame = cap.read()

    # If the frame is recieved
    if(ret == True):

        # Taking Gray Scale image
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Show the video frame
        cv2.imshow('Video Feed', gray)    

        # Write the frame to the video
        #Output.write(gray)

        # If the S button is pressed exit the video
        if((cv2.waitKey(1) & 0xFF) == ord('s')):        
            break;

    # Else break and exit the while loop
    else:
        break

# Disable the camera and free up all the occupies resources
cap.release()        

# Stop writing video in the file
#Output.release() 

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()                            