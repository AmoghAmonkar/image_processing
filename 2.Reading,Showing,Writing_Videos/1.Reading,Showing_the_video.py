import cv2

# Creating a object Cap of Class Video capture to read an existing video
cap = cv2.VideoCapture('Images/Megamind.avi')

# If the video has opened succesfully then execute this
while(cap.isOpened()):

    # ret recieves if the image is recieved or not (True / False)
    # frame captures image frame recieved from video
    ret, frame = cap.read()

    # If the frame is recieved
    if(ret == True):
        # Show the video frame
        cv2.imshow('Video Feed', frame) 

        # If the S button is pressed exit the video
        if((cv2.waitKey(1) & 0xFF) == ord('s')):
            break
    
    # Else break and exit the while loop
    else:
        break


# Disable the camera and free up all the occupies resources
cap.release()

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()