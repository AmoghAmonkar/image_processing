import cv2

# Path to the image to be read
Img_path = 'Images/detect_blob.png'

# Read the image
img = cv2.imread(Img_path)

# Show the image
cv2.imshow("Image to be displayed", img)

# Record the ASCII of the key pressed
K = cv2.waitKey(0) & 0xFF

# Destroy all the windows displaying imagess
cv2.destroyAllWindows()

# If S key is pressed save the image as follwing
if(K == ord("S") or K == ord("s") ):
    cv2.imwrite("1.Reading,Showing,Writing_Images/copy_of_detect_blob.png", img)