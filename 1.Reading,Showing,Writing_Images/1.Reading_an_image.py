# Import OpenCV
import cv2                      

# This function returns a image matrix if correct path is provided else returns none
#   Flag   -   Meaning
#    1     -   Coloured Image
#    0     -   Grayscale image
#   -1     -   Image with Alpha Channel

img = cv2.imread('/media/amogh/WORK/Python_Files/ImageProcessing/image_processing/Images/apple.jpg', 1)

print(img)