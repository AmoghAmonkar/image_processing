import numpy as np
import cv2

# Creating a callback function for the trackbars to call while update 
def nothing(x):
    pass

# Creating a black image
img = np.zeros((512, 512, 3), np.uint8)

# Creating named window called 'Image'
cv2.namedWindow('Image')

# Creating the trackbars on the image windows
cv2.createTrackbar('B', 'Image', 0, 255, nothing)           # Trackbar for manipulating Blue channel               
cv2.createTrackbar('G', 'Image', 0, 255, nothing)           # Trackbar for manipulating Green channel
cv2.createTrackbar('R', 'Image', 0, 255, nothing)           # Trackbar for manipulating Red channel

switch = '0 - OFF\n1 - ON'
cv2.createTrackbar(switch, 'Image', 0, 1, nothing)          # Switch to activate trackbars

# Infinte loop for continuous updating the window
while(True):

    # Displaying the image
    cv2.imshow('Image', img)

    # Getting the values from the trackbars 
    b = cv2.getTrackbarPos('B', 'Image')
    g = cv2.getTrackbarPos('G', 'Image')
    r = cv2.getTrackbarPos('R', 'Image')

    # Get value of the switch
    Button = cv2.getTrackbarPos(switch, 'Image')

    # If the switch is on get values from the trackbars
    if(Button == 1):
        # Update the values of the all the pixels of the image
        img[:] = [b, g, r]
    else:
        # Set the values of the all the pixels of the image to 0
        img[:] = 0

    # If the S button is pressed exit the video
    if((cv2.waitKey(1) & 0xFF) == ord('s')):
        break

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows() 