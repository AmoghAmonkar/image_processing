import numpy as np
import cv2

# Creating a callback function for the trackbars to call while update 
def nothing(x):
    pass

# Infinte loop for continuous updating the window
while(True):

    # Creating a black image
    img = cv2.imread('Images/smarties.png')

    # Converting from coloured image to HSV image
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # Setting upper and lower limits of the blue color
    l_b = np.array([110, 50, 50])
    u_b = np.array([130, 255, 255])

    # Making a binary image
    mask = cv2.inRange(hsv, l_b, u_b)

    # Bitwise ANDing the image using mask
    res = cv2.bitwise_and(img, img, mask=mask)

    # Displaying the image
    cv2.imshow('Image', img)
    cv2.imshow('mask', mask)
    cv2.imshow('result', res)

    # If the S button is pressed exit the video
    if((cv2.waitKey(1) & 0xFF) == ord('s')):
        break

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows() 