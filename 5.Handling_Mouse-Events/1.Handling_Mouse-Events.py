import numpy as np
import cv2

'''
For listing down the available events in the cv2
    events = [for i indir(cv2) if 'EVENT' in i]
    print(events)
'''

'''
#   Name        :   Mouse_Click_Event(event, x, y, flags, param)
#   Parameters  :   obj event, x and y coordinates, flags, parameters
#   Return Type :   none (void)
#   Logic       :   It is a callback function in case if the the event occurs
#                   1. Identify the event that hsa occured
#                   2. Execute corresponding sequence
'''
def Mouse_Click_Event(event, x, y, flags, param):

    # If the left button is clicked
    if(event == cv2.EVENT_LBUTTONDOWN):
        # Setting the font to be used for the printting the date and time
        font = cv2.FONT_HERSHEY_SIMPLEX        
        # Creating a string contaning the x, y coordinates of the point which event has occured
        coord =  '(' + str(x) + ', ' + str(y) + ')'
        # Pasting the text on the image 
        cv2.putText(img, coord, (x, y), font, 0.3, (255, 255, 255), 1, cv2.LINE_AA)
        # Show the image
        cv2.imshow('Image', img)

    # If the right button is clicked
    if(event == cv2.EVENT_RBUTTONDOWN):
        # Blue channel retrieval of the pixel
        b = img[y, x, 0] 
        # Green channel retrieval of the pixel
        g = img[y, x, 1] 
        # Red channel retrieval of the pixel
        r = img[y, x, 2] 
        # Setting the font to be used for the printting the date and time
        font = cv2.FONT_HERSHEY_SIMPLEX        
        # Creating a string contaning the x, y coordinates of the point which event has occured
        coord =  '(' + str(b) + ', ' + str(g) + ', ' + str(r) + ')'
        # Pasting the text on the image 
        cv2.putText(img, coord, (x, y), font, 0.3, (0, 0, 0), 1, cv2.LINE_AA)
        # Show the image
        cv2.imshow('Image', img)

# Creating a Total black image of dim 1920x1080
img = cv2.imread('Images/Parrot.jpg')

# Show the image
cv2.imshow('Image', img)

# Trigger the callback function if the any of the event occurs
cv2.setMouseCallback('Image', Mouse_Click_Event)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')): 
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()  