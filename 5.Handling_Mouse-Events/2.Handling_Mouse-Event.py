import numpy as np
import cv2

'''
For listing down the available events in the cv2
    events = [for i indir(cv2) if 'EVENT' in i]
    print(events)
'''

'''
#   Name        :   Mouse_Click_Event(event, x, y, flags, param)
#   Parameters  :   obj event, x and y coordinates, flags, parameters
#   Return Type :   none (void)
#   Logic       :   It is a callback function in case if the the event occurs
#                   1. Identify the event that hsa occured
#                   2. Execute corresponding sequence
'''
def Mouse_Click_Event(event, x, y, flags, param):

    # If the left button is clicked
    if(event == cv2.EVENT_LBUTTONDOWN):
        # Pasting the text on the image 
        cv2.circle(img, (x, y), 2, (255, 255, 255), -1)
        # Add the point to the list of points
        points.append((x, y))
        # If more than two points join them with a line
        if(len(points) >= 2):
            cv2.line(img, points[-1], points[-2], (255, 255, 255), 3)
        # Show the image
        cv2.imshow('Image', img)

    # If the right button is clicked
    if(event == cv2.EVENT_RBUTTONDOWN):
        # Blue channel retrieval of the pixel
        b = img[y, x, 0] 
        # Green channel retrieval of the pixel
        g = img[y, x, 1] 
        # Red channel retrieval of the pixel
        r = img[y, x, 2] 
        # Pasting the text on the image 
        cv2.circle(img, (x, y), 1, (255, 255, 255), -1)
        # Creating a empty black image
        clr_img = np.zeros((50, 50, 3), np.uint8)
        # Colour the whole image with selected colour
        clr_img[:] = [b, g, r]
        # Show the image
        cv2.imshow('Chosen_Color', clr_img)

# Creating a Total black image of dim 1920x1080
#img = np.zeros((1000, 1000, 3), np.uint8)
img = cv2.imread('Images/Wolf.jpg')

# Show the image
cv2.imshow('Image', img)

# Creating an empty list to store all the points on which the mouse is clicked
points = []

# Trigger the callback function if the any of the event occurs
cv2.setMouseCallback('Image', Mouse_Click_Event)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')): 
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()  