import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('Images/sudoku.png', 0)

# Lalacian of the images Images
lap = cv2.Laplacian(img, cv2.CV_64F, ksize=1)
lap = np.uint8(np.absolute(lap))

# Sobel X Gradients
sobelX = cv2.Sobel(img, cv2.CV_64F, 1, 0)
sobelX = np.uint8(np.absolute(sobelX))

# Sobel Y Gradients
sobelY = cv2.Sobel(img, cv2.CV_64F, 0, 1)
sobelY = np.uint8(np.absolute(sobelY))

# Combining the X and Y Gradients
sobelXY = cv2.bitwise_or(sobelX, sobelY)

Titles = ['Images', 'Laplacian', 'Sobel X', 'Sobel Y', 'Sobel X Y']
Images = [img, lap, sobelX, sobelY, sobelXY]

# Loop for showing images using Matplotlib
for i in range(len(Images)):
    plt.subplot(2, 3, i+1)
    plt.imshow(Images[i], 'gray')
    plt.title(Titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()