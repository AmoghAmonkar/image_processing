import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('Images/messi5.jpg', 0)

# Canny edge detection
canny = cv2.Canny(img, 100, 200)

Titles = ['Images', 'Canny']
Images = [img, canny]

# Loop for showing images using Matplotlib
for i in range(len(Images)):
    plt.subplot(1, 2, i+1)
    plt.imshow(Images[i], 'gray')
    plt.title(Titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()