import cv2

# Reading an image
Test_img = cv2.imread('Images/shapes_1.jpg')

# Reading the image as gray scale image
Grey_img = cv2.imread('Images/shapes_1.jpg', 0)

# Showing the images
cv2.imshow('Image',Test_img)
cv2.imshow('Grey_Image',Grey_img)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()