import cv2

# Creating a object of VideoCapture Class to read the video
cap = cv2.VideoCapture('Images/vtest.avi')

#cap = cv2.VideoCapture(0)

# Read the first frame from the video feed
ret, frame_1 = cap.read()

# Read the second frame from the video feed
ret, frame_2 = cap.read()

# Continuosly read the new images
while cap.isOpened():

    # Taking the difference between the two images
    diff = cv2.absdiff(frame_1, frame_2)

    # Converting the difference between the images into the grayscale so that we can detect the contours from the image
    gray_diff = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)

    # Blurring the images for noise removal
    Blur = cv2.GaussianBlur(gray_diff, (5, 5), 0)

    # Thresholding of the images
    _, Thresh = cv2.threshold(Blur, 20, 255, cv2.THRESH_BINARY)

    # Dilation of the images which helps in better contour detection
    Dilated = cv2.dilate(Thresh, None, iterations=3)

    # Contour Detection
    cnts, _ = cv2.findContours(Dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # Drawing the contours
    #cv2.drawContours(frame_1, cnts, -1, (0, 0, 255), 2)

    # To avoid un-necessary contours setting a area condition
    for cnt in cnts:

        # Function which returns rectangle width and height
        (x, y, w, h) = cv2.boundingRect(cnt)

        # Area bounded by the contour is  less than dont mark it
        if(cv2.contourArea(cnt) < 1300):
            continue
        cv2.rectangle(frame_1, (x, y), (x+w, y+h), (0, 255, 0), 2)

    # Showing the video frame
    cv2.imshow('Video_Feed', frame_1)

    # Updating the frame to be processed
    frame_1 = frame_2

    # Taking new frame for reference
    ret, frame_2 = cap.read()

    # If the S button is pressed exit the video
    if((cv2.waitKey(1) & 0xFF) == ord('s')):
        # Break from the while loop
        break

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()
# Kill the created videocapture object
cap.release()