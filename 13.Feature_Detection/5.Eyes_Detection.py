import cv2

# Creating a cascade classfier object for face detection
Eye_Casscade = cv2.CascadeClassifier('Haarcascades/haarcascade_eye.xml')

# Reading the Input Image
img = cv2.imread('Images/lena.jpg')

# Converting to the grayscale
gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Extracting faces from the image
Eyes = Eye_Casscade.detectMultiScale(gray_img, 1.1, 4)

# Annotate the face with a reactangle
for (X, Y, W, H) in Eyes:
    # Draw reactangle in the image
    cv2.rectangle(img, (X, Y), (X+W, Y+H), (0, 255, 0), 2)

# Displaying the images
cv2.imshow('Eye_Recognition', img)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()