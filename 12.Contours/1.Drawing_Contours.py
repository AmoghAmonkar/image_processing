import cv2

# Reading the image in grayscale
img = cv2.imread('Images/opencv-logo.png', 1)

gray = cv2.imread('Images/opencv-logo.png', 0)

# Thresholding the image
ret, Thresh = cv2.threshold(gray, 127, 255, 0)

# Contour Identification
cnt, h = cv2.findContours(Thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

# Printing the number of Contours
print('Number of Contours = ' + str(len(cnt)))

# Drawing contours
cv2.drawContours(img, cnt, -1, (0, 0, 255), 2)

# Displaying the input image
cv2.imshow('Input_Image', img)

# Displaying the porcessed_image
cv2.imshow('Thresholded_Image', Thresh)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()