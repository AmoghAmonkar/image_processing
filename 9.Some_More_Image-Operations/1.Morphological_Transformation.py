import cv2
import numpy as np
from matplotlib import pyplot as plt

# Reading the image in grayscale
img = cv2.imread('Images/smarties.png', 0)

# Creating a masks for 
ret, mask = cv2.threshold(img, 220, 255, cv2.THRESH_BINARY_INV)

# Create a kernal for morphological transformation
kernel = np.ones((2, 2), np.uint8)

# Dilate the image
dilate = cv2.dilate(mask, kernel, iterations=2)

# Erode the image
erode = cv2.erode(mask, kernel, iterations=1)

# Opening the image
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)

# Closing the image
closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

# Morphological Gradient
mg = cv2.morphologyEx(mask, cv2.MORPH_GRADIENT, kernel)

# Top Hat
th = cv2.morphologyEx(mask, cv2.MORPH_TOPHAT, kernel)

# Lists of Title 
Titles = ['Image', 'Mask', 'Dilated', 'Erosion', 'Opening', 'Closing', 'Morph_Grad', 'Top_Hat']

# Lists of Images
Images = [img, mask, dilate, erode, opening, closing, mg, th]

# Loop for showing images using Matplotlib
for i in range(len(Images)):
    plt.subplot(2, 3, i+1)
    plt.imshow(Images[i], 'gray')
    plt.title(Titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()