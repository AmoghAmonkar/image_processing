import numpy as np
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('Images/lena.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# Kernel for blurring images
kernel = np.ones((5, 5), np.uint32) / 25

# Filtered Images
dst = cv2.filter2D(img, -1, kernel)

# Blurring the image
blur = cv2.blur(img, (5, 5))

# Gaussian Blurred Images
g_blur = cv2.GaussianBlur(img, (5, 5), 0)

# Median Blurred Image second parameter needs to be odd except 1
m_blur = cv2.medianBlur(img, 5)

# Bilateral Filter
b_blur = cv2.bilateralFilter(img, 9, 75, 75)

Titles = ['Images', '2D Convolution', 'Blurred', 'Gaussian Blurred', 'Median Blur', 'Bilateral Filter']
Images = [img, dst, blur, g_blur, m_blur, b_blur]

# Loop for showing images using Matplotlib
for i in range(len(Images)):
    plt.subplot(2, 3, i+1)
    plt.imshow(Images[i], 'gray')
    plt.title(Titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()