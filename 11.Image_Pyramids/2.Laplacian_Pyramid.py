import numpy as np
import cv2

# Reading the image
img = cv2.imread('Images/Parrot.jpg')

# Creating the copy of the image
layer = img.copy()

# Creating a list of the images
gp = [layer]

# For loop for the creating list of images in the Gaussian Pyramids
for i in range(2):
    layer = cv2.pyrDown(layer)
    gp.append(layer)

# Assignnigg the last element to the layer
layer = gp[-1]

cv2.imshow('Topmost Gaussian Pyramid', layer)

lp = [layer]

# For loop for the creating list of images in the Laplacian Pyramids
for i in range(2, 0, -1):
    gauss_extend = cv2.pyrUp(gp[i])
    laplacian = cv2.subtract(gp[i-1], gauss_extend)
    cv2.imshow(str(i), laplacian)

# Displaying the default image
cv2.imshow('Original Image', img)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()
