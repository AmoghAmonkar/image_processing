import numpy as np
import cv2

# Reading the image
img = cv2.imread('Images/Parrot.jpg')

# Creating the copy of the image
layer = img.copy()

# Creating a list of the images
gp = [layer]

# For loop for the creating list of images in the Gaussian Pyramids
for i in range(2):
    layer = cv2.pyrDown(layer)
    gp.append(layer)
    cv2.imshow(str(i), layer)

# Displaying the default image
cv2.imshow('Original Image', img)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()
