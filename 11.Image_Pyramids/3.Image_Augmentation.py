import cv2
import numpy as np

A = cv2.imread('Images/apple.jpg')
O = cv2.imread('Images/orange.jpg')

# Slicing both the images and joining them
A_O = np.hstack((A[:,:256], O[:, 256:]))

# Displaying the image
cv2.imshow("Apple and Orange", A_O)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()
