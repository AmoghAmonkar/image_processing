import cv2
import numpy as np

A = cv2.imread('Images/apple.jpg')
O = cv2.imread('Images/orange.jpg')

# Generate Gaussian Pyramids for both the images
A_copy = A.copy()
O_copy = O.copy()

gp_apple = [A_copy]
gp_orange = [O_copy]

for i in range(6):
    A_copy = cv2.pyrUp(A_copy)
    O_copy = cv2.pyrUp(O_copy)
    gp_apple.append(A_copy)
    gp_orange.append(O_copy)

# Generate Laplacian Pyramids for both the images 
A_copy = gp_apple[-1]
lp_apple = [A_copy]
O_copy = gp_orange[-1]
lp_orange = [O_copy]

for i in range(5, 0, -1):
    gauss_extend_apple = cv2.pyrUp(gp_apple[i])
    gauss_extend_orange = cv2.pyrUp(gp_orange[i])

    laplacian_apple = cv2.subtract(gp_apple[i-1], gauss_extend_apple)
    laplacian_orange = cv2.subtract(gp_orange[i-1], gauss_extend_orange)

    lp_apple.append(laplacian_apple)
    lp_orange.append(laplacian_orange)

# Now add left and right halves of images in each level
A_O_pyramid = []
n = 0

for apple_lap, orange_lap in zip(lp_apple, lp_orange):
    n += 1
    cols, rows, ch = apple_lap.shape
    laplacian = np.hstack((apple_lap[:, 0:int(cols/2)], orange_lap[:, int(cols/2):]))
    A_O_pyramid.append(laplacian)
    
# now reconstruct
A_O_reconstruct = A_O_pyramid[0]
for i in range(1, 6):
    A_O_reconstruct = cv2.pyrUp(A_O_reconstruct)
    A_O_reconstruct = cv2.add(A_O_pyramid[i], A_O_reconstruct)

# Showing the images
cv2.imshow("apple", A)
cv2.imshow("orange", O)
cv2.imshow("apple_orange_reconstruct", A_O_reconstruct)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()