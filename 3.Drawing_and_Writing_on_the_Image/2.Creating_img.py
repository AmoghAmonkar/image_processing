import numpy as np
import cv2

img = np.zeros([640, 512, 3], np.uint8)

line_img = cv2.line(img, (0, 0), (512, 512), (0, 255, 255), 1)       

Arr_line = cv2.arrowedLine(img, (0, 512), (512, 512), (0, 255, 0), 2)

Rect_Img = cv2.rectangle(img, (200, 200), (390, 390), (255, 0, 0), 1)

Circ_Img = cv2.circle(img, (285, 285), 85, (255, 255, 255), 2)

Text_Img = cv2.putText(img, "OpenCV", (200, 450), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 2)

cv2.imshow("Image", img) 

if((cv2.waitKey(0) & 0xFF) == 27):
    cv2.destroyAllWindows()