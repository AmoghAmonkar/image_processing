import cv2

Img_Path = 'Images/lena.jpg'                                                # Path of the image to be displayed

img = cv2.imread(Img_Path, 1)                                               # Reading an image

cp_img = img                                                                # Creating copy of the images

line_img = cv2.line(cp_img, (0, 0), (512, 512), (0, 255, 255), 1)           # Drawing an line of yellow color

#cv2.imshow("Line_Image", line_img)                                         # Show the image

Arr_line = cv2.arrowedLine(cp_img, (0, 256), (256, 256), (0, 255, 0), 2)

#cv2.imshow("Arrow_Image", Arr_line) 

Rect_Img = cv2.rectangle(cp_img, (200, 200), (390, 390), (255, 0, 0), 1)

#cv2.imshow("Rect_Image", Rect_Img) 

Circ_Img = cv2.circle(cp_img, (285, 285), 85, (0, 0, 0), 2)

#cv2.imshow("Rect_Image", Circ_Img) 

Text_Img = cv2.putText(cp_img, "OpenCV", (200, 450), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 2)

cv2.imshow("Rect_Image", Text_Img) 

if((cv2.waitKey(0) & 0xFF) == 27):
    cv2.destroyAllWindows()