import numpy as np
import cv2

# Creating a callback function for the trackbars to call while update 
def nothing(x):
    pass

# Creating an object for camera feed
cap = cv2.VideoCapture(0)

# Creating named window called 'Image'
cv2.namedWindow('Tracking')

# Creating the the trackbars for adjusting the thresholds
cv2.createTrackbar('LH', 'Tracking', 0, 255, nothing)
cv2.createTrackbar('LS', 'Tracking', 0, 255, nothing)
cv2.createTrackbar('LV', 'Tracking', 0, 255, nothing)
cv2.createTrackbar('UH', 'Tracking', 255, 255, nothing)
cv2.createTrackbar('US', 'Tracking', 255, 255, nothing)
cv2.createTrackbar('UV', 'Tracking', 255, 255, nothing)

frame_no = 0 
Initial_Frame = 0

# Infinte loop for continuous updating the window
while(True):

    # Tracking number of frames
    frame_no += 1

    # Storing the image in the frame
    ret, frame = cap.read()

    if(frame_no==1):
        Initial_Frame = frame

    # Converting from coloured image to HSV image
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # Setting upper and lower limits of all the color
    # Lower Limits of HSV parameters
    l_H = cv2.getTrackbarPos('LH', 'Tracking')
    l_S = cv2.getTrackbarPos('LS', 'Tracking')
    l_V = cv2.getTrackbarPos('LV', 'Tracking')

    # Upper Limits of HSV parameters
    U_H = cv2.getTrackbarPos('UH', 'Tracking')
    U_S = cv2.getTrackbarPos('US', 'Tracking')
    U_V = cv2.getTrackbarPos('UV', 'Tracking')

    # Setting upper and lower limits of the blue color
    l_b = np.array([l_H, l_S, l_V])
    u_b = np.array([U_H, U_S, U_V])

    # Making a binary image
    mask = cv2.inRange(hsv, l_b, u_b)
    inverted_mask = cv2.bitwise_not(mask)

    # Blurred MAsk
    blurred_mask = cv2.bilateralFilter(inverted_mask,9,75,75)

    # Bitwise ANDing the image using mask
    AND_img_1 = cv2.bitwise_and(frame, frame, mask=blurred_mask)

    #Final Composite Image
    AND_img_2 = cv2.bitwise_and(Initial_Frame, Initial_Frame, mask=mask)

    res = AND_img_1 + AND_img_2

    # Displaying the image
    cv2.imshow('Image', frame)
    cv2.imshow('mask', mask)
    cv2.imshow('result', res)

    # If the S button is pressed exit the video
    if((cv2.waitKey(1) & 0xFF) == ord('s')):
        break

# Releasing the camera object
cap.release()

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows() 