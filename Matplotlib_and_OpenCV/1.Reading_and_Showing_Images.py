import cv2
from matplotlib import pyplot as plt

# Reading a image and storing it into a variable using OpenCV
img = cv2.imread('Images/Wolf.jpg')

# Showing the image
# Using OpenCV
cv2.imshow('Image_In_OpenCV', img)

# Converting the image to RGB format since Matplotlib uses RGB format to Read the images
RGB_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# Using Matplotlib
plt.imshow(RGB_img)
plt.show()

# Wait
cv2.waitKey(0) 
# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()