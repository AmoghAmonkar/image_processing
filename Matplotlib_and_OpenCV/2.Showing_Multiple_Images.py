import cv2
import numpy as np
from matplotlib import pyplot as plt

# Reading the image in grayscale
img = cv2.imread('Images/gradient.png', 0)

# Binary Thresholding the image
ret, thres_1 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)

# Inverse Binary Thresholding the image
ret, thres_2 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)

# Truncation Thresholding
ret, thres_3 = cv2.threshold(img, 100, 255, cv2.THRESH_TRUNC)

# T0 Zero Thresholding
ret, thres_4 = cv2.threshold(img, 100, 255, cv2.THRESH_TOZERO)

# T0 Zero Thresholding
ret, thres_5 = cv2.threshold(img, 100, 255, cv2.THRESH_TOZERO_INV)

Titles = ['Original Images', 'Binary', 'Binary_INV', 'Trunc', 'ToZero', 'ToZeor_INV']
Images = [img, thres_1, thres_2, thres_3, thres_4, thres_5]

for i in range(len(Images)):
    plt.subplot(2, 3, i+1)
    plt.imshow(Images[i], 'gray')
    plt.title(Titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()