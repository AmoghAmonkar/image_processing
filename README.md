# Image_Processing

Implementation of Various image processing algorithms using OpenCV library in python.

## Reading, Writing and Showing the image

### Reading an Image

**Function used:**

```python
# This function returns a image matrix if correct path is provided else returns none
img = imread('path-to-the',Flag)
```

|Flag   |Meaning                            |
|:-----:|:----------------------------------|
|1      |Coloured Image                     |
|0      |Gray Scale Image                   |
|-1     |Coloured Image with Alpha Channel. |

### Showing and Writing the Image

**Functions Used for showing images:**

```python
# Show the image
cv2.imshow("Image to be displayed", img)

# Record the ASCII of the key pressed
K = cv2.waitKey(0) & 0xFF

# Destroy all the windows displaying imagess
cv2.destroyAllWindows()
```

**Functions Used for writing images:**

```python
cv2.imwrite("copy_of_detect_blob.png", img)
```

## Reading, Writing and Showing the video

### Reading and showing a pre-existing video

**Problem Faced:** The video does not show up on the screen or only first frame of the video showsup.

**Solution:** Make sure you have given 1 second delay in the cv2.waitKey() function.

```python
import cv2

# Creating a object Cap of Class Video capture to read an existing video
cap = cv2.VideoCapture('Images/Megamind.avi')

# If the video has opened succesfully then execute this
while(cap.isOpened()):

    # ret recieves if the image is recieved or not (True / False)
    # frame captures image frame recieved from video
    ret, frame = cap.read()

    # If the frame is recieved
    if(ret == True):
        # Show the video frame
        cv2.imshow('Video Feed', frame)

        # If the S button is pressed exit the video
        if((cv2.waitKey(1) & 0xFF) == ord('s')):
            break

    # Else break and exit the while loop
    else:
        break


# Disable the camera and free up all the occupies resources
cap.release()

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()
```

### Reading and showing a video from the WebCam

Following function is used to inintiate video feed from the WebCam:

```python
# Creating a object Cap of Class Video capture to read video from WebCam
cap = cv2.VideoCapture(0)
```

**Note:** The VideoCapture() function takes Camera id as a parameter
|Id     |Camera                             |
|:-----:|:----------------------------------|
|   -1  |Default Camera (WebCam)            |
|   0   |Default Camera (WebCam)            |
|   1   |First External WebCam              |
|   2   |Second External WebCam             |

```python
import cv2

# Creating a object Cap of Class Video capture to read video from WebCam
cap = cv2.VideoCapture(0)

# If the video has opened succesfully then execute this
while(cap.isOpened()):

    # ret recieves if the image is recieved or not (True / False)
    # frame captures image frame recieved from video
    ret, frame = cap.read()

    # If the frame is recieved
    if(ret == True):

        # Taking Gray Scale image
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Show the video frame
        cv2.imshow('Video Feed', gray)

        # If the S button is pressed exit the video
        if((cv2.waitKey(1) & 0xFF) == ord('s')):
            break;

    # Else break and exit the while loop
    else:
        break

# Disable the camera and free up all the occupies resources
cap.release()

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()
```

## Drawing and Writing on the Images

### Drawing a Line

```python
pt1 = (x1, y1)
pt2 = (x2, y2)
clr = (0, 255, 255)
thickness = 1

# Drawing an line
line_img = cv2.line(src_img, pt1, pt2, clr, thickness)
```

### Drawing a Arrowed Line

```python
pt1 = (x1, y1)
pt2 = (x2, y2)
clr = (0, 255, 255)
thickness = 1

# Drawing an arrowed line
Arr_line = cv2.arrowedLine(src_img, pt1, pt2, clr, thickness)
```

### Drawing a Rectangle

```python
pt1 = (x1, y1)
pt2 = (x2, y2)
clr = (0, 255, 255)
thickness = 1

# Drawing a Rectangle
Rect_Img = cv2.rectangle(src_img, pt1, pt2, clr, thickness)
```

### Drawing a Circle

```python
ctr = (x1, y1)
rad = r
clr = (0, 255, 255)
thickness = 1

# Drawing a Circle
Circ_Img = cv2.circle(cp_img, ctr, rad, clr, thickness)
```

**Note:** For closed shapes such as rectangle and circle thickness = -1 means that th shap will be filled by the colour.

### Writing Text on the on the image

```python
import cv2

start = (x1, y1)
Str = 'Text to be written'
font = cv2.FONT_HERSHEY_PLAIN
fnt_size = 4
clr = (255, 255, 255)
thickness = 1

# Writing a text on the image
Text_Img = cv2.putText(src_img, Str, start, font, fnt_size, clr, thickness)
```

### Creating Image using Numpy

```python
import numpy as np

# Create a black image of width = 640 height = 512 and 3 channels
img = np.zeros([640, 512, 3], np.uint8)
```

## Setting Camera Parameters

### Changing Camera settings

**Note:** You cannot set width and height of the width greater the maximum resolution

```python
'''
    Name of the Parameter       Number Associated
    cv2.CAP_PROP_FRAME_WIDTH            3
    cv2.CAP_PROP_FRAME_HEIGHT           4
'''

# Setting the Frame Width of the video to 1920 pixels
cap.set(3, 1920)

# Setting the Frame Height of the video to 1080 pixels
cap.set(4, 1080)
```

### Printing a timestamp and date on the video

```python
import cv2
import datetime as dt

# Creating a object Cap of Class Video capture to read video from WebCam
cap = cv2.VideoCapture(0)

# If the video has opened succesfully then execute this
while(cap.isOpened()):

    # ret recieves if the image is recieved or not (True / False)
    # frame captures image frame recieved from video
    ret, frame = cap.read()

    # If the frame is recieved
    if(ret == True):

        # Setting the font to be used for the printting the date and time
        font = cv2.FONT_HERSHEY_SIMPLEX

        # Str object containig the date and time of today
        dnt = str(dt.datetime.now())

        # PAsting the text on the image
        cv2.putText(frame, dnt, (400, 470), font, 0.5, (255, 255, 255), 1, cv2.LINE_AA)

        # Show the video frame
        cv2.imshow('Video Feed', frame)

        # If the S button is pressed exit the video
        if((cv2.waitKey(1) & 0xFF) == ord('s')):
            break;

    # Else break and exit the while loop
    else:
        break

# Disable the camera and free up all the occupies resources
cap.release()

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()
```

## Handling mouse events

### Left and Right click events and images

```python
import numpy as np
import cv2

'''
For listing down the available events in the cv2
    events = [for i indir(cv2) if 'EVENT' in i]
    print(events)
'''

'''
#   Name        :   Mouse_Click_Event(event, x, y, flags, param)
#   Parameters  :   obj event, x and y coordinates, flags, parameters
#   Return Type :   none (void)
#   Logic       :   It is a callback function in case if the the event occurs
#                   1. Identify the event that hsa occured
#                   2. Execute corresponding sequence
'''
def Mouse_Click_Event(event, x, y, flags, param):

    # If the left button is clicked
    if(event == cv2.EVENT_LBUTTONDOWN):
        # Setting the font to be used for the printting the date and time
        font = cv2.FONT_HERSHEY_SIMPLEX
        # Creating a string contaning the x, y coordinates of the point which event has occured
        coord =  '(' + str(x) + ', ' + str(y) + ')'
        # Pasting the text on the image
        cv2.putText(img, coord, (x, y), font, 0.3, (255, 255, 255), 1, cv2.LINE_AA)
        # Show the image
        cv2.imshow('Image', img)

    # If the right button is clicked
    if(event == cv2.EVENT_RBUTTONDOWN):
        # Blue channel retrieval of the pixel
        b = img[y, x, 0]
        # Green channel retrieval of the pixel
        g = img[y, x, 1]
        # Red channel retrieval of the pixel
        r = img[y, x, 2]
        # Setting the font to be used for the printting the date and time
        font = cv2.FONT_HERSHEY_SIMPLEX
        # Creating a string contaning the x, y coordinates of the point which event has occured
        coord =  '(' + str(b) + ', ' + str(g) + ', ' + str(r) + ')'
        # Pasting the text on the image
        cv2.putText(img, coord, (x, y), font, 0.3, (0, 0, 0), 1, cv2.LINE_AA)
        # Show the image
        cv2.imshow('Image', img)

# Creating a Total black image of dim 1920x1080
img = cv2.imread('Images/Parrot.jpg')

# Show the image
cv2.imshow('Image', img)

# Trigger the callback function if the any of the event occurs
cv2.setMouseCallback('Image', Mouse_Click_Event)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()  
```

## Operations on the Image

### Adding two images

```python
import numpy as np
import cv2

# Reading the images and storing it in the variables
img_1 = cv2.imread('Images/messi5.jpg')
img_2 = cv2.imread('Images/opencv-logo.png')

# Spliting the image into Blue, Green and Red Channel
b, g, r = cv2.split(img_1)

# From the individual channels image can be reformed
img_1 = cv2.merge((b, g, r))

# Separating the Region Of Intrest (R.O.I) by slicing the image along the x and y axes
ball = img_1[280:340, 330:390]

# Overwriting the R.O.I on the exiting image
img[273:333, 100:160] = ball

# Before adding two images the images need to be of same sizes
img_1 = cv2.resize(img_1, (512, 512))
img_2 = cv2.resize(img_2, (512, 512))

# Adding the images
final_img_1 = cv2.add(img_1, img_2)
final_img_2 = cv2.addWeighted(img_1, 0.9, img_2, 0.1)

# Showing the images
cv2.imshow('Image_1', final_img_1)
cv2.imshow('Image_2', final_img_2)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()
```

### Bitwise Operation On two images

```python
import numpy as np
import cv2

# Reading the images and storing it in the variables
img_1 = cv2.imread('Images/messi5.jpg')
mask = np.zeros((img_1.shape[0], img_1.shape[1], 3), np.uint8)

# Drawing the white rectangle on the black image
cv2.rectangle(mask, (int(0.25*img_1.shape[0]), int(0.25*img_1.shape[1])), (int(0.75*img_1.shape[0]), int(0.75*img_1.shape[1])), (255, 255, 255), -1)

# Bitwise Operation
#AND_img = cv2.bitwise_and(mask, img_1)
#OR_img = cv2.bitwise_or(mask, img_1)
#XOR_img = cv2.bitwise_xor(mask, img_1)
NOT_img = cv2.bitwise_not(mask)

# Showing the images
#cv2.imshow('Img_1', img_1)
cv2.imshow('Mask', mask)
cv2.imshow('NOT_Img', NOT_img)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()  
```

## Creating a Trackbar and using it

```python
import numpy as np
import cv2

# Creating a callback function for the trackbars to call while update
def nothing(x):
    pass

# Creating a black image
img = np.zeros((512, 512, 3), np.uint8)

# Creating named window called 'Image'
cv2.namedWindow('Image')

# Creating the trackbars on the image windows
cv2.createTrackbar('B', 'Image', 0, 255, nothing)           # Trackbar for manipulating Blue channel
cv2.createTrackbar('G', 'Image', 0, 255, nothing)           # Trackbar for manipulating Green channel
cv2.createTrackbar('R', 'Image', 0, 255, nothing)           # Trackbar for manipulating Red channel

switch = '0 - OFF\n1 - ON'
cv2.createTrackbar(switch, 'Image', 0, 1, nothing)          # Switch to activate trackbars

# Infinte loop for continuous updating the window
while(True):

    # Displaying the image
    cv2.imshow('Image', img)

    # Getting the values from the trackbars
    b = cv2.getTrackbarPos('B', 'Image')
    g = cv2.getTrackbarPos('G', 'Image')
    r = cv2.getTrackbarPos('R', 'Image')

    # Get value of the switch
    Button = cv2.getTrackbarPos(switch, 'Image')

    # If the switch is on get values from the trackbars
    if(Button == 1):
        # Update the values of the all the pixels of the image
        img[:] = [b, g, r]
    else:
        # Set the values of the all the pixels of the image to 0
        img[:] = 0

    # If the S button is pressed exit the video
    if((cv2.waitKey(1) & 0xFF) == ord('s')):
        break

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows()
```

## Thresholding

**Types of Thresholding:**

- **Binary Thresholding:** If the value of the pixel is greater than the threshold it is re-assigned as 1 if it is less then it is re-assignmed as 0. It can be applied only on a grayscale image.

```python
# Reading the image in grayscale
img = cv2.imread('Images/gradient.png', 0)

# Thresholding the image
ret, thres_1 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('Thresholded_Image', thres_1)
```

- **Inverse Binary Thresholding:** It is the reverse logic of Binary Thresholding. If the value of the pixel is greater than the threshold it is re-assigned as 0 if it is less then it is re-assignmed as 1. It can be applied only on a grayscale image.

```python
# Reading the image in grayscale
img = cv2.imread('Images/gradient.png', 0)

# Inverse Binary Thresholding the image
ret, thres_2 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('INV_hresholded_Image', thres_2)
```

- **Truncation Thresholding:** The values of the pixels will remain as it is till the threshold value and  pixels whose value is greater than threshold is trucated to the threshold. It can be applied only on a grayscale image.

```python
# Reading the image in grayscale
img = cv2.imread('Images/gradient.png', 0)

# Truncation Thresholding
ret, thres_3 = cv2.threshold(img, 100, 255, cv2.THRESH_TRUNC)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('Trunc_Thresholded_Image', thres_3)
```

- **To Zero Thresholding:** All the pixels with values less than that of the threshold is set to zero. It can be applied only on a grayscale image.

```python
# Reading the image in grayscale
img = cv2.imread('Images/gradient.png', 0)

# T0 Zero Thresholding
ret, thres_4 = cv2.threshold(img, 100, 255, cv2.THRESH_TOZERO)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('T0Zero_Thresholded_Image', thres_4)
```

- **Inverse To Zero Thresholding:** Opposite to the To Zero Thresholding. All the pixels with values greater than that of the threshold is set to zero. It can be applied only on a grayscale image.

```python
# Reading the image in grayscale
img = cv2.imread('Images/gradient.png', 0)

# T0 Zero Thresholding
ret, thres_5 = cv2.threshold(img, 100, 255, cv2.THRESH_TOZERO_INV)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('INV_T0Zero_Thresholded_Image', thres_4)
```

- **Adaptive Threshlding:** Sometimes using a global threshold is not a good technique since all the part of image do not have same light intensity. So the better way is to use adaptive threshold which calculates custom threshold for the small region of the images by using statistical methods such as Mean or Gaussian.

```python
# Reading the image in grayscale
img = cv2.imread('Images/sudoku.png', 0)

# Binary Thresholding the image
ret, thres_1 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)

# Adaptive thresholding the image using Mean C method
ad_thres_1 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)

# Adaptive thresholding the image using Gaussian C method
ad_thres_2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('BIN_Thresholded_Image', thres_1)
cv2.imshow('Adaptive_Mean_Thresholded_Image', ad_thres_1)
cv2.imshow('Adaptive_Gaussian_Thresholded_Image', ad_thres_2)
```

## Morphological Transformation

These are some simple operations based on the image shape. These operations can only be applied on binary images. This transformation involves kernel which tells how to change the value of any given pixel by combining it with different amount of the neighbouring pixel.

1. **Erosion:** It erodes away the boundaries of foreground object. The kernel slides through the image. A pixel in the original image (either 1 or 0) will be considered 1 only if all the pixels under the kernel is 1, otherwise it is eroded (made to zero).

2. **Dilation:** It is the opposite of erosion.

3. **Opening:** Erosion followed by dilation.

4. **Closing:** Dilation followed by erosion.

5. **Morphological Gradient:** Difference between dilation and erosion.

6. **Top Hat:** Difference between image and opening of the image.

- [For more information on morphological trasnformation](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_morphological_ops/py_morphological_ops.html).

## Smoothening Images

- **Homogeneous Filter:** It is the most simple filter, each output pixel is the mean of itd kernel neighbours.I

- **Gaussian Filter:** It uses gaussian weighted kernel in x and y direction. Used to remove high frequency noise from the image.

- **Median Filter:** Replaces each pixels with the median pixel of the from the kernel. Used to remove salt pepper noise.

- **Bilateral Filter:** It used to preserve borders and smoothen out rest all the part of the images.

**Note:**

- Low Pass Filter helps in removing noises and blurring the images.
- High Pass Filter helps in finding edges in the images.

## Images Gradient and Edge Detection

**Images Gradient:** Direction of the change in the intensity or color in an image.

**Sobel X & Y:** Detects edge along the X and Y direction.

## Image Pyramids

Type of a multiscale signal representation in which image is subject to reapeted smothing and subsampling.

## Contour

Lines joining pointof same colour, intensity etc. It works better on a grayscale image.

```python
import cv2

# Reading the image in grayscale
img = cv2.imread('Images/opencv-logo.png', 1)

gray = cv2.imread('Images/opencv-logo.png', 0)

# Thresholding the image
ret, Thresh = cv2.threshold(gray, 127, 255, 0)

# Contour Identification
cnt, h = cv2.findContours(Thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

# Printing the number of Contours
print('Number of Contours = ' + str(len(cnt)))

# Drawing contours
cv2.drawContours(img, cnt, -1, (0, 0, 255), 2)

# Displaying the input image
cv2.imshow('Input_Image', img)

# Displaying the porcessed_image
cv2.imshow('Thresholded_Image', Thresh)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()
```

## References

- [OpenCV Documentation](https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html)
