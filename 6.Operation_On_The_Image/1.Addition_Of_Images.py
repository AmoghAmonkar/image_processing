import numpy as np
import cv2

# Reading the images and storing it in the variables
img_1 = cv2.imread('Images/messi5.jpg')
img_2 = cv2.imread('Images/opencv-logo.png')

# Spliting the image into Blue, Green and Red Channel
b, g, r = cv2.split(img_1)

# From the individual channels image can be reformed
img_1 = cv2.merge((b, g, r))

# Separating the Region Of Intrest (R.O.I) by slicing the image along the x and y axes
ball = img_1[280:340, 330:390]

# Overwriting the R.O.I on the exiting image
img_1[273:333, 100:160] = ball

# Before adding two images the images need to be of same sizes
img_1 = cv2.resize(img_1, (512, 512))
img_2 = cv2.resize(img_2, (512, 512))

# Adding the images
final_img_1 = cv2.add(img_1, img_2)
final_img_2 = cv2.addWeighted(img_1, 0.9, img_2, 0.1, 0)

# Showing the images
cv2.imshow('Image_1', final_img_1)
cv2.imshow('Image_2', final_img_2)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')): 
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()  