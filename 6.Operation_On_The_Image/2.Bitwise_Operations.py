import numpy as np
import cv2

# Reading the images and storing it in the variables
img_1 = cv2.imread('Images/messi5.jpg')
mask = np.zeros((img_1.shape[0], img_1.shape[1], 3), np.uint8)

# Drawing the white rectangle on the black image
cv2.rectangle(mask, (int(0.25*img_1.shape[0]), int(0.25*img_1.shape[1])), (int(0.75*img_1.shape[0]), int(0.75*img_1.shape[1])), (255, 255, 255), -1)

# Bitwise Operation
AND_img = cv2.bitwise_and(mask, img_1)
#OR_img = cv2.bitwise_or(mask, img_1)
#XOR_img = cv2.bitwise_xor(mask, img_1)
#NOT_img = cv2.bitwise_not(mask)

# Showing the images
cv2.imshow('Img_1', img_1)
cv2.imshow('Mask', mask)
cv2.imshow('NOT_Img', AND_img)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')): 
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()  