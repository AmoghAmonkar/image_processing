import numpy as np 
import cv2

# Reading the image in grayscale
img = cv2.imread('Images/gradient.png', 0)

# Binary Thresholding the image
ret, thres_1 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)

# Inverse Binary Thresholding the image
ret, thres_2 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY_INV)

# Truncation Thresholding
ret, thres_3 = cv2.threshold(img, 100, 255, cv2.THRESH_TRUNC)

# T0 Zero Thresholding
ret, thres_4 = cv2.threshold(img, 100, 255, cv2.THRESH_TOZERO)

# T0 Zero Thresholding
ret, thres_5 = cv2.threshold(img, 100, 255, cv2.THRESH_TOZERO_INV)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('T0Zero_Thresholded_Image', thres_4)
cv2.imshow('INV_T0Zero_Thresholded_Image', thres_5)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()