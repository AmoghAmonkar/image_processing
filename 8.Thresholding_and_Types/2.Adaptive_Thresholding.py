import numpy as np 
import cv2

# Reading the image in grayscale
img = cv2.imread('Images/sudoku.png', 0)

# Binary Thresholding the image
ret, thres_1 = cv2.threshold(img, 127, 255, cv2.THRESH_BINARY)

# Adaptive thresholding the image using Mean C method
ad_thres_1 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)

# Adaptive thresholding the image using Gaussian C method
ad_thres_2 = cv2.adaptiveThreshold(img, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

# Showing the image
cv2.imshow('Image', img)
cv2.imshow('BIN_Thresholded_Image', thres_1)
cv2.imshow('Adaptive_Mean_Thresholded_Image', ad_thres_1)
cv2.imshow('Adaptive_Gaussian_Thresholded_Image', ad_thres_2)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()