import cv2
import numpy as np

# Reading and resizing the image 
path = 'Images/home.jpg'
img = cv2.imread(path)
img = cv2.resize(img,(400,400))

# Converting the image to grayscale
gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)

# Computing the threshold of the grayscale image
_,thresh = cv2.threshold(gray, np.mean(gray), 255, cv2.THRESH_BINARY_INV)

# Using canny edge detection and then dilating the image
edges = cv2.dilate(cv2.Canny(thresh,0,255),None)

# Store the largest contour
cnt = sorted(cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2], key=cv2.contourArea)[-1]

# Create a mask of 0 pixel of the dimensions of the original image
mask = np.zeros((400,400), np.uint8)

# Draw the detected contour on the image 
masked = cv2.drawContours(mask, [cnt],-1, 255, -1)

# And the image with the mask
dst = cv2.bitwise_and(img, img, mask=mask)

# Displaying the image
cv2.imshow('Result',dst)

# If the S button is pressed exit the video
if((cv2.waitKey(0) & 0xFF) == ord('s')):
    # Destroy all the windows popped for the displaying image
    cv2.destroyAllWindows()