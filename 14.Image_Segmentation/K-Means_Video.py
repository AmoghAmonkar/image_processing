import numpy as np
import cv2

# Creating a callback function for the trackbars to call while update 
def nothing(x):
    pass

# Creating an object for camera feed
cap = cv2.VideoCapture(0)

# Infinte loop for continuous updating the window
while(True):
    # Storing the image in the frame
    ret, frame = cap.read()

    # reshape the image to an array of Mx3 size (M is number of pixels in image)
    Z = frame.reshape((-1,3))

    # convert to np.float32
    Z = np.float32(Z)

    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 3
    ret,label,center=cv2.kmeans(Z,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)

    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((frame.shape))

    cv2.imshow('res2',res2)

    # If the S button is pressed exit the video
    if((cv2.waitKey(1) & 0xFF) == ord('s')):
        break

# Releasing the camera object
cap.release()

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows() 