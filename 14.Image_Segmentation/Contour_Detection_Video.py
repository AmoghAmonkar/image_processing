import cv2
import numpy as np

# Creating a callback function for the trackbars to call while update 
def nothing(x):
    pass

# Creating an object for camera feed
cap = cv2.VideoCapture(0)

# Infinte loop for continuous updating the window
while(True):
    # Storing the image in the frame
    ret, frame = cap.read()

    # Resizing the frame
    img = cv2.resize(frame,(500,500))

    # Converting the image to grayscale
    gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)

    # Computing the threshold of the grayscale image
    _,thresh = cv2.threshold(gray, np.mean(gray), 255, cv2.THRESH_BINARY_INV)

    # Using canny edge detection and then dilating the image
    edges = cv2.dilate(cv2.Canny(thresh,0,255),None)

    # Store the largest contour
    cnt = sorted(cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2], key=cv2.contourArea)[-1]

    # Create a mask of 0 pixel of the dimensions of the original image
    mask = np.zeros((500,500), np.uint8)

    # Draw the detected contour on the image 
    masked = cv2.drawContours(mask, [cnt],-1, 255, -1)

    # And the image with the mask
    dst = cv2.bitwise_and(img, img, mask=masked)

    # Displaying the image
    cv2.imshow('Result', dst)

    # If the S button is pressed exit the video
    if((cv2.waitKey(1) & 0xFF) == ord('s')):
        break

# Releasing the camera object
cap.release()

# Destroy all the windows popped for the displaying image
cv2.destroyAllWindows() 